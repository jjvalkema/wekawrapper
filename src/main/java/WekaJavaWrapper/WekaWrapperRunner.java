/*
 * Copyright (c) 2019 Jimjim Valkema
 * All rights reserved
 */

package WekaJavaWrapper;

import java.util.*;

public class WekaWrapperRunner {
    public static final  String header = "\"huml\",\"humw\",\"ulnal\",\"ulnaw\",\"feml\",\"femw\",\"tibl\",\"tibw\",\"tarl\",\"tarw\"";
    public static final double[] means = {64.87487,4.381235,69.19741,3.606538,36.82245,3.221768,64.61564,3.182324,39.30981,2.944891};
    public static final double[] sd = {54.05608,2.86146,58.92949,2.19015,19.89936,2.021169,37.97793,2.084124,23.2727,2.194744};
    public static void main(String[] args) {

        try {
            ApacheCliOptionsProvider op = new ApacheCliOptionsProvider(args);
            if (op.helpRequested() || op.haNosUserOptions()) {
                op.printHelp();
                return;
            } else {
                //TODO make optional
                String normalizedCsv = "normalised.csv";
                Normalizer normalizer = new Normalizer(means,sd,header);
                //normalise from either the terminal or CSV/arff and store the output into a csv
                if (op.instanceProvided()) {
                    System.out.println("classifying instance from terminal");
                    String instance = op.getInstance();
                    normalizer.createNormCsvFromInstance(instance, normalizedCsv);
                } else {
                    String inputFile = op.getInputfile();
                    if (inputFile.toLowerCase().endsWith(".csv")) {
                        normalizer.createNormCsvFromCsvFile(inputFile, normalizedCsv);
                    } else if (inputFile.toLowerCase().endsWith("arff")) {
                        normalizer.createNormCsvFromArfFile(inputFile,normalizedCsv);
                    } else {throw new IllegalAccessException("File type is unknown");}
                    System.out.println("classifying instance from CSV");
                }
                Classifier classifier = new Classifier("SW,W,T,R,P,SO");
                classifier.classifyCsv(normalizedCsv);
            }
        } catch (Throwable ex) {
            System.err.println("Something went wrong while processing your command line \""
                    + Arrays.toString(args) + "\"");
            System.err.println("Parsing failed.  Reason: " + ex.getMessage());
            ApacheCliOptionsProvider op = new ApacheCliOptionsProvider(new String[]{});
            op.printHelp();
        }
    }
}
