package WekaJavaWrapper;

import java.io.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * A object with functions to normalize a arff or csv file or single instance to a new csv file.
 */
public class Normalizer {
    private double[] means;// = new double[10]; //{64.87487,4.381235,69.19741,3.606538,36.82245,3.221768,64.61564,3.182324,39.30981,2.944891};
    private double[] sd; //{54.05608,2.86146,58.92949,2.19015,19.89936,2.021169,37.97793,2.084124,23.2727,2.194744};\
    private String header;

    /**
     * A object with functions to normalize a arff or csv file or single instance to a new csv file.
     * @param means a list of means as doubles used for scaling normalization
     * @param sd a list of standard deviations as doubles used for scaling normalization
     * @param header a comma separated header for the output csv or also input file if it is a csv
     */
    public Normalizer(double[] means, double[] sd, String header) {
        this.setMeans(means);
        this.setSd(sd);
        this.setHeader(header);
    }

    /**
     * does a log2 transform
     * @param x :  a double to be transformed
     * @return
     */
    private static double log2(double x)
    {
        return (Math.log(x) / Math.log(2));
    }

    /**
     * @param data : as a string[]
     * @return : a String with a items comma separated
     */
    private static String convertToCSV(String[] data) {
        return Stream.of(data).collect(Collectors.joining(","));
    }

    /**
     * Normalises one instance from a String[] and returns it a list<String>
     * @param instance :  the instance as a String
     * @return : normalised instance as a list<String>
     */
    private  String[] normalizeInstance(String instance) throws NumberFormatException {
        double[] validInstance = getValidInstance(instance);
        double res;
        String[] normalizedInstance = new String[10];
        for (int i = 0; i < validInstance.length; i ++) {
            res = log2(validInstance[i]);
            res = (res - this.means[i]) / this.sd[i];
            normalizedInstance[i] = String.valueOf(res);
        }
        return normalizedInstance;
    }

    /**
     * Creates a csv file with a given header. over writes if it already exists
     * @param outputFilePath : The file path to create the file at as a String
     * @param header : The header as a string
     * @return :  the file as a buffer
     */
    private BufferedWriter createCsvTemplateFile(String outputFilePath) {
        try {
            //Create normalized file
            File outputFile = new File(outputFilePath);
            BufferedWriter outPutFile = new BufferedWriter(
                    new FileWriter(outputFilePath, true));
            outPutFile.write(this.getHeader() + "\n");
            if (outputFile.createNewFile()) {
            } else {
                PrintWriter writer = new PrintWriter(outputFile);
                writer.print("");
                writer.close();
            }
            return outPutFile;
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    /**
     * Validates and parses a given attribute and throws IllegalArgumentExceptions if the instance contains invalid attributes
     * @param attribute The attribute as a string to parse and validate
     * @return attribute the parsed and validated attribute as a double
     */
    private static double getValidAttribute(String attribute) {
        double attributeAsDouble;
        try {
            attributeAsDouble = Double.parseDouble(attribute);
        } catch (NumberFormatException e) {
            throw new IllegalArgumentException("Attribute: \"" + attribute + "\" is invalid, attribute is not a number.");
        }
        if (attributeAsDouble <= 0) {
            throw new IllegalArgumentException("Attribute: \"" + attribute + "\" is invalid, attributes can't be negative or zero");
        }
        return  attributeAsDouble;
    }

    /**
     * Validates and parses a given instance and throws IllegalArgumentExceptions if the instance contains invalid attributes
     * @param instance The attribute as a string to parse and validate
     * @return attribute the parsed and validated attribute as a double[]
     */
    private static double[] getValidInstance(String instance) {
        double[] instanceAsDouble = new double[10];
        String[] instanceAsString = instance.split(",");
        if (instanceAsString.length < 10) {
            throw new IllegalArgumentException("Instance is missing attributes. Instance has "
                    + instanceAsString.length  +" attributes but needs to have 10");
        } else if (instanceAsString.length > 10) {
            throw new IllegalArgumentException("Instance contains too many attributes. Instance has "
                    + instanceAsString.length  +" attributes but needs to have 10");
        }
        for (int i = 0; i < instanceAsString.length; i++) {
            instanceAsDouble[i] = getValidAttribute(instanceAsString[i]);
        }
        return instanceAsDouble;
    }

    /**
     * @param inputFile input file arff as a file path String
     * @param outputCsv output file csv as a file path String
     */
    public void createNormCsvFromArfFile(String inputFile, String outputCsv) {
        String line;
        try {
            BufferedReader br = new BufferedReader(new FileReader(inputFile));
            //Create normalized file
            BufferedWriter outputFile = createCsvTemplateFile(outputCsv);
            if ((br.readLine()) == null) {
                throw new IllegalArgumentException("file is empty");
            }
            while ((line = br.readLine()) != null) {
                if (!(line.startsWith("@") || line.isBlank())) {
                    String[] normCsv = normalizeInstance(line);
                    //Write Content
                    String newCSVLine = convertToCSV(normCsv) + "\n";
                    outputFile.write(newCSVLine);
                }
            }
            outputFile.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * normalises from a csv file and creates a new normalised csv file.
     * @param csvFile :  filepath to the csv file that needs to be normalised
     * @param outputFilePath : filepath to the output csv file that is normalised
     */
    public void createNormCsvFromCsvFile(String csvFile, String outputFilePath) {

        BufferedReader br = null;
        String line;
        String[] rowData;

        try {
            //Create normalized file
            BufferedWriter outputFile = createCsvTemplateFile(outputFilePath);

            //read input
            br = new BufferedReader(new FileReader(csvFile));

            if ((line = br.readLine()) == null) {
                throw new IllegalArgumentException("file is empty");
            }
            else if (line.equals("")) {
                throw new IllegalArgumentException("header is empty. Expected: "+ this.getHeader());
            }
            else if (line.equals(header) != true) {
                throw new IllegalArgumentException("Input file doesn't contain the right header. Expected: " + header
                        + "\n But got: " +  line);
            }
            while ((line = br.readLine()) != null) {

                rowData = normalizeInstance(line);

                //Write Content
                String newCSVLine = convertToCSV(rowData)+"\n";
                outputFile.write(newCSVLine);
            }
            outputFile.close();

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (br != null) {
                try {
                    br.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }

    }

    /**
     * Normalises one instance and puts it a csv file
     * @param instance : a single instance as a String[] with 10 atributes
     * @param outputFilePath : The file path for the normalised output as a csv
     */
    public void createNormCsvFromInstance(String instance, String outputFilePath){
        String[] normInstance = normalizeInstance(instance);

        BufferedWriter outPutFile = createCsvTemplateFile(outputFilePath);
        String newCSVLine = convertToCSV(normInstance)+"\n";

        try {
            outPutFile.write(newCSVLine);
            outPutFile.close();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }


    private void setMeans(double[] means) {
        this.means = means;
    }

    private void setSd(double[] sd) {
        this.sd = sd;
    }

    public double[] getMeans() {
        return means;
    }

    public double[] getSd() {
        return sd;
    }

    public String getHeader() {
        return header;
    }

    public void setHeader(String header) {
        this.header = header;
    }
}
