package WekaJavaWrapper;

import weka.classifiers.meta.Stacking;
import weka.core.Instances;
import weka.core.converters.CSVLoader;
import weka.filters.Filter;
import weka.filters.unsupervised.attribute.Add;

import java.io.File;

public class Classifier {
    private String classLabels;

    public Classifier(String classLabels) {
        this.classLabels = classLabels;
    }

    /**
     * classifies from a normalised csv and outputs the results in the terminal
     * @param normalizedCsv :  The filepath to the normalised csv as a String
     */
    public void classifyCsv(String normalizedCsv) {

        try {
            Stacking classifierFromFile = loadClassifier();
            CSVLoader csvLoader = new CSVLoader();
            csvLoader.setSource(new File(normalizedCsv));
            csvLoader.setNominalAttributes("first-last");
            Instances unClassifiedInstancesFromCsv = csvLoader.getDataSet();
            if (unClassifiedInstancesFromCsv.classIndex() == -1)
                unClassifiedInstancesFromCsv.setClassIndex(unClassifiedInstancesFromCsv.numAttributes() - 1);


            //define attribute and its labels, name etc
            Instances newData = new Instances(unClassifiedInstancesFromCsv);
            // 1. nominal attribute
            Add filter = new Add();
            filter.setAttributeIndex("last");
            filter.setNominalLabels(this.classLabels);
            filter.setAttributeName("type");
            filter.setInputFormat(newData);
            newData = Filter.useFilter(newData, filter);

            newData.setClassIndex(newData.numAttributes() - 1);

            classifyNewInstance(classifierFromFile, newData);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * classifies instances and outputs the results in the terminal
     * @param stack : The stack machine learning model as a Stack object
     * @param unClassifiedInstances : a Instances object that contains the instances
     * @throws Exception
     */
    public void classifyNewInstance(Stacking stack, Instances unClassifiedInstances) throws Exception {
        // create copy
        Instances labeled = new Instances(unClassifiedInstances);
        // label instances
        for (int i = 0; i < unClassifiedInstances.numInstances(); i++) {
            double clsLabel = stack.classifyInstance(unClassifiedInstances.instance(i));
            labeled.instance(i).setClassValue(clsLabel);
        }
        for (int i = 0; i < labeled.size(); i++) {
            System.out.println("instance "+ (i+1) + " classified as: " + labeled.get(i).stringValue(10));
            System.out.println("With the probability distribution of: ");
            double[] distributionForInstance = stack.distributionForInstance(labeled.instance(i));
            for (int j = 0; j < distributionForInstance.length; j++) {
                System.out.println(this.classLabels.split(",")[j] + ": " + distributionForInstance[j]);
            }
        }
    }

    /**
     * Loads the model from a .model file and creates a Stacking 0bject
     * @return
     * @throws Exception
     */
    private Stacking loadClassifier() throws Exception {
        // deserialize model
        ClassLoader classLoader = getClass().getClassLoader();
        File file = new File("MetaStackingMaxDepth12.model");

        return (Stacking) weka.core.SerializationHelper.read(file.getAbsolutePath());
    }
}
