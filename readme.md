# Bird habitat classifier  
This program uses a pre-trained classical machine learning algorithm created with weka to classify the living habitat
of birds with their bone measurements. 

#### Bone measurements used  
The following bone measurements have been used on the length and diameter of the bones.
hum(l/w):  Humerus (mm)  
ulna(l/w): Ulna (mm)  
fem(l/w):  Femur (mm)  
tib(l/w):  Tibiotarsus (mm)  
tar(l/w):  Tarsometatarsus (mm)  


## requirements 
Java 11 or openjdk 11

## Running the program  
To run the program as a .jar:  
download this entire folder: wekawrapper/build/libs/  
Open command line and use cd to go to where you downloaded the libs folder.      
`cd /path/to/libs/folder`  

#### to run on a csv/arff file  
Type the following to run the program but change insertPathToYourFile.csv to the path or either your csv or arff.   
`java -jar javaWrapper-1.0-SNAPSHOT-all.jar -f insertPathToYourFile.csv`

#### run on a single instance   
type the following into the command line but change the values after the "-i" flag to run on your own instance.  
`java -jar javaWrapper-1.0-SNAPSHOT-all.jar -i 19.21,1.64,20.76,1.49,19.24,1.45,33.21,1.28,23.6,1.15`    

## Program options  
This program has the following options   
-h show help  
-f input csv/arff file   
-i classify a single instance  

## input format  
The csv file has to be comma separated and have the following header: "huml","humw","ulnal","ulnaw","feml","femw","tibl","tibw","tarl","tarw"  
The arff file needs to have the attributes in the following order: huml,humw,ulnal,ulnaw,feml,femw,tibl,tibw,tarl,tarw  

## output  
The program will output the instance number and its classification as well as it's probability distribution.  
The program will also make a file named normalised.csv which contains the input file that is log2 transformed and normalised with scaling normalisation.

